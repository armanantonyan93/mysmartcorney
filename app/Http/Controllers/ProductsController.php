<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Excel;
use App\Models\Products;
use App\imports\ProductsImport;
use App\Http\Requests\ProductRequest;

class ProductsController extends Controller
{
    public function  HomePage(){

        $products = Products::paginate(15);


        return view('frontend.Home',compact('products'));
    }

    public function FilterPrice(request $request){

            $minprice = $request->minprice;

            $maxprice = $request->maxprice;

            $products = Products::where('price','>=',$minprice)->where('price','<=',$maxprice)->get();

            if($request->has('new')){

                    $products = $products->orderBy('created_at', 'desc')->orderBy('something', 'asc');

            }

            if($request->has('old')){
                $products = $products->orderBy('created_at', 'asc')->orderBy('something', 'desc');
            }



            return view('frontend.Home',compact('products'));

    }

    public function CreateProduct(){
        return view('create');
    }
    public function  Store(ProductRequest $request){
        $validated = $request->validated($request->all());

        Products::create($validated);

        return redirect('/home');
    }
    public function UpdateProduct($id){
        $product = Products::find($id);
        return view('edit',compact('product'));
    }
    public function Update(ProductRequest $request,$id){
        $product = Products::findOrFail($id);

        $validated = $request->validated();

        $product->fill($validated)->save();

        return redirect('/home');
    }
    public function ImportCsv(Request $request){
        Excel::import(new ProductsImport,$request->file);

        return redirect('/home');
    }
    public function DestroyProduct($id){
        $Product = Products::findOrFail($id);

        $Product->delete();

        return redirect('/home');
    }
}
