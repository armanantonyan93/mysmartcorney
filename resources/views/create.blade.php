@extends('layouts.app')

@section('content')
    <div class="container-sm">
    <form action="{{route('ProductStore')}}" method="POST" enctype="multipart/form-data" style="width: 60%;">

        @csrf
        @if($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{$error}}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <div class="form-group">
            <label for="exampleInputName">Name</label>
            <input type="text" class="form-control" name="name" placeholder="Name">
            <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
        </div>
        <div class="form-group">
            <label for="exampleInputdescription">Description</label>
            <input type="text" class="form-control" id="exampleInputdescription" name="description" placeholder="description">
        </div>
        <div class="form-group">
            <label for="exampleInputdescription">Price $</label>
            <input type="number" class="form-control" name="price">
        </div>
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
    <h2>Or Import CSV</h2>
    <form action="{{Route('ImportCsv')}}" method="POST" enctype="multipart/form-data">
        @csrf
        <input type="file" name="file">
        <input type="submit" value="Import">
    </form>
    </div>
@endsection
