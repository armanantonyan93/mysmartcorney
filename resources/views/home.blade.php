@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Dashboard') }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                        @foreach ($products as $product)

                                <h5 class="card-title">{{$product->name}}</h5>
                                <p class="card-text">{{substr($product->description,0,50)}}</p>

                                <a href="{{route('UpdateProduct',$product->id)}}" class="btn btn-primary">Edit</a>

                                <form method="{{route('DeleteProduct',$product->id)}}" method="DELETE" style="margin-top: 10px;">
                                    @method('delete')
                                    @csrf
                                    <input type="submit" class="btn btn-danger" value="Delete">
                                </form>
                            </br>

                        @endforeach

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
