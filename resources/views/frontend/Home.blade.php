@extends('frontend.app')
@section('content')
    <main class="main-content-solutions">
        <form method="get" action="{{route('FilterPrice')}}" style="margin:5em;">
            <input type="number" name="minprice" value="min" style="width:150px;">
            <input type="number" name="maxprice" value="max" style="width:150px;">

            <label>Date (new-old)</label>
            <input type="radio" name="new">

            <label>Date (old-new)</label>
            <input type="radio" name="old">

            <input type="submit" class="btn" value="Filter with price">
        </form>
        @foreach($products as $product)
        <section class="solutions-top-box">
            <div class="solutions-top-box-inner">
                <h1>{{$product->name}}</h1>
                <p>{{substr($product->name,0,30)}}</p>
            </div>
        </section>
        @endforeach
    </main>
@endsection
