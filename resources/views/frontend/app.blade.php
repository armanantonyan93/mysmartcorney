<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0 user-scalable=0">
    <title>Optify</title>
    <link rel="stylesheet" href="{{asset('css/style.css')}}">
    <link rel="stylesheet" href="{{asset('css/solutions.css')}}">
    <link href="https://fonts.googleapis.com/css2?family=Francois+One&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@400;500;700&display=swap" rel="stylesheet">
    <link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon">
    <link rel="icon" href="img/favicon.ico" type="image/x-icon">
</head>
<body>
<header class="nav-bar">
    <div class="logo-box">
        <a href="{{Route('homepage')}}">
        <p>StoreHouse<br><span>SmartCorner task</span></p></a>
    </div>
    <button class="menu-open is_active">
        <svg id="Component_6_4" data-name="Component 6 – 4" xmlns="http://www.w3.org/2000/svg" width="69" height="26" viewBox="0 0 69 26">
            <rect id="Rectangle_372" data-name="Rectangle 372" width="69" height="10" rx="5" fill="#1c1c1c"/>
            <rect id="Rectangle_373" data-name="Rectangle 373" width="53" height="10" rx="5" transform="translate(16 16)" fill="#1c1c1c"/>
        </svg>
    </button>
    <ul class="nav-bar-main">
        <li>
            <a href="{{route('homepage')}}">Products</a>
        </li>
        @if(Auth::check() == true)
                Welcome {{Auth::user()->name}}
        @else
            <li class="with-button">
                <a href="/login"> Login</a>
            </li>
        @endif
    </ul>
</header>
@yield('content')
<footer>
    <ul class="footer-inner">
        <li class="left-box">
            <h4>Follow Us</h4>
            <div>
                <svg xmlns="http://www.w3.org/2000/svg" width="19.12" height="36.828" viewBox="0 0 19.12 36.828">
                    <path id="Path_183" data-name="Path 183" d="M222.223,93.7c.009,0,.009-.009.018-.009a2.6,2.6,0,0,1,1.564-.536c1.438.036,2.877.063,4.315.1V86.8h-5.807a7.963,7.963,0,0,0-3.065.59,7.411,7.411,0,0,0-2.895,2.117,7.634,7.634,0,0,0-1.706,5.209v5.593H209v6.683h5.656v16.636h6.737V106.992h5.557l1.027-6.683h-6.585V95.475A2.241,2.241,0,0,1,222.223,93.7Z" transform="translate(-209 -86.8)" fill="#553dff"/>
                </svg>
                <svg xmlns="http://www.w3.org/2000/svg" width="28.876" height="30.47" viewBox="0 0 28.876 30.47">
                    <g id="Group_520" data-name="Group 520" transform="translate(-10.594 -9)">
                        <rect id="Rectangle_281" data-name="Rectangle 281" width="7" height="22" transform="translate(10.594 17)" fill="#553dff"/>
                        <path id="Path_52" data-name="Path 52" d="M24.839,11c-4.406,0-5.164,1.607-5.524,3.158V11H13V33.1h6.315V20.473a2.838,2.838,0,0,1,3.158-3.158,2.849,2.849,0,0,1,3.158,3.158V33.1h6.315V22.052C31.946,15.736,31.123,11,24.839,11Z" transform="translate(7.524 6.367)" fill="#553dff"/>
                        <circle id="Ellipse_123" data-name="Ellipse 123" cx="3.5" cy="3.5" r="3.5" transform="translate(10.594 9)" fill="#553dff"/>
                    </g>
                </svg>
                <svg xmlns="http://www.w3.org/2000/svg" width="35.934" height="35.934" viewBox="0 0 35.934 35.934">
                    <g id="Group_521" data-name="Group 521" transform="translate(-284.294 -5442.293)">
                        <path id="Path_47" data-name="Path 47" d="M29.473,38.662a9.189,9.189,0,1,1,9.19-9.19A9.2,9.2,0,0,1,29.473,38.662Zm0-15.9a6.716,6.716,0,1,0,6.717,6.714A6.723,6.723,0,0,0,29.473,22.758Z" transform="translate(272.787 5430.788)" fill="#553dff"/>
                        <ellipse id="Ellipse_122" data-name="Ellipse 122" cx="1.143" cy="1.143" rx="1.143" ry="1.143" transform="translate(311.624 5448.956)" fill="#553dff"/>
                        <path id="Path_48" data-name="Path 48" d="M42.611,52.669H26.794A10.069,10.069,0,0,1,16.735,42.612V26.794A10.069,10.069,0,0,1,26.794,16.735H42.611A10.069,10.069,0,0,1,52.669,26.794V42.612A10.069,10.069,0,0,1,42.611,52.669ZM26.794,19.208a7.6,7.6,0,0,0-7.585,7.586V42.612A7.594,7.594,0,0,0,26.794,50.2H42.611A7.594,7.594,0,0,0,50.2,42.612V26.794a7.6,7.6,0,0,0-7.585-7.586Z" transform="translate(267.559 5425.559)" fill="#553dff"/>
                    </g>
                </svg>
                <svg xmlns="http://www.w3.org/2000/svg" width="30.213" height="22.667" viewBox="0 0 30.213 22.667">
                    <g id="Group_311" data-name="Group 311" transform="translate(-1046 -7042)">
                        <path id="Path_103" data-name="Path 103" d="M29.911,8.891a7.265,7.265,0,0,0-1.2-3.2,4.168,4.168,0,0,0-3.027-1.363C21.455,4,15.113,4,15.113,4H15.1S8.757,4,4.53,4.328A4.169,4.169,0,0,0,1.5,5.691a7.277,7.277,0,0,0-1.2,3.2A51.915,51.915,0,0,0,0,14.107v2.444a51.9,51.9,0,0,0,.3,5.216,7.25,7.25,0,0,0,1.2,3.2,4.941,4.941,0,0,0,3.331,1.378c2.417.247,10.273.324,10.273.324s6.349-.011,10.577-.335a4.186,4.186,0,0,0,3.027-1.366,7.263,7.263,0,0,0,1.2-3.2,51.921,51.921,0,0,0,.3-5.216V14.108A51.921,51.921,0,0,0,29.911,8.891Z" transform="translate(1046 7038)" fill="#553dff"/>
                        <path id="Path_104" data-name="Path 104" d="M12,10V21.333l9.445-5.667Z" transform="translate(1045.333 7037.667)" fill="#fcfbff"/>
                    </g>
                </svg>
            </div>
        </li>
        <li class="right-box">
            <h4>Subscribe Us</h4>
            <input placeholder="Email Address" class="email-address">
            <button>GO!</button>
        </li>
    </ul>
    <span class="line"></span>
    
</footer>
</body>
</html>
