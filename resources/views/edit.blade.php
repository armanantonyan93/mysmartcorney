@extends('layouts.app')

@section('content')
    <div class="container-sm">
        <form action="{{route('ProductStore')}}" method="POST" enctype="multipart/form-data" style="width: 60%;">

            @csrf
            @if($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{$error}}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="form-group">
                <label for="exampleInputName">Name</label>
                <input type="text" class="form-control" name="name" value="{{$product->name}}">
                <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
            </div>
            <div class="form-group">
                <label for="exampleInputdescription">Description</label>
                <input type="text" class="form-control" id="exampleInputdescription" value="{{substr($product->description,0,20)}}" name="description" placeholder="description">
            </div>
            <div class="form-group">
                <label for="exampleInputdescription">Price $</label>
                <input type="number" name="price" class="form-control" value="{{$product->price}}">
            </div>
            <button type="submit" class="btn btn-primary">Update Product</button>
        </form>


    </div>
@endsection
