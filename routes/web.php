<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ProductsController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [ProductsController::class,'HomePage'])->name('homepage');

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('/home/filterprice', [ProductsController::class, 'FilterPrice'])->name('FilterPrice');

Route::get('/product/create',[ProductsController::class,'CreateProduct'])->name('CreateProduct');

Route::post('/product/store',[ProductsController::class,'Store'])->name('ProductStore');
Route::get('/product/update/{id}',[ProductsController::class,'UpdateProduct'])->name('UpdateProduct');
Route::delete('/delete/product/{id}',[ProductsController::class,'DestroyProduct'])->name('DeleteProduct');
Route::post('/product/store/inputcsv',[ProductsController::class,'ImportCsv'])->name('ImportCsv');

